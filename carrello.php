<?php 
    include("top.html");
    include("./common.php");

    if(!isset ($_SESSION["id_utente"])){
        header("location: ./index.php");
        die();
    }
?>

<div class="container" id="homeContainer">

    <div class="jumbotron" id="jumbotron">
        <h1 class="display-4">Questo è il tuo carrello</h1>
        <hr class="my-4">
        <p class="lead">Qui c'è la lista dei prodotti che hai aggiunto al carrello.</p>
    </div>
    
</div>

<script src="js/manager_script.js"></script>
<script src="js/load_prodotti.js"></script>
<script src="js/logout_script.js"></script>
<?php include("bottom.html"); ?>