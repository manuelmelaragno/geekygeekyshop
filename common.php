<?php 
    session_set_cookie_params(0);
    session_start();   

    if(isset($_POST["logout"])){
        unset($_SESSION["id_utente"]);
        unset($_SESSION["email"]);
        unset($_SESSION["nome"]);
        session_destroy();
    }

function connect(){
    $db = new PDO("mysql:dbname=geekygeekyshop;host=localhost;charset=utf8", "root", "");
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    return $db;
}

?>