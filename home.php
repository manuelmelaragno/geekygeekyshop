<?php 
    include("top.html");
    include("./common.php");

    if(!isset ($_SESSION["id_utente"])){
        header("location: ./index.php");
        die();
    }
?>
<div class="container" id="homeContainer">

    <div class="jumbotron" id="jumbotron">
        <h1 class="display-4">Benvenuto nel nostro e-commerce</h1>
        <hr class="my-4">
        <p class="lead">Questa è la home, per prima cosa scegli un catalogo da sfogliare!</p>
    </div>
    <a href="films.php">
        <div class="carta" id="catFilm">
            <div class="imgCat"></div>
            <h2>Film</h2>
            <p>Raccolta di film che puoi acquistare ad un prezzo scontato</p>
        </div>
    </a>
    <a href="./serietv.php">
        <div class="carta" id="catSerieTv">
            <div class="imgCat"></div>
            <h2>Serie TV</h2>
            <p>Raccolta di Serie TV, acquistale e scaricale in alta risoluzione!</p>
        </div>
    </a>    
    <a href="./videogiochi.php">
        <div class="carta" id="catVideogiochi">
            <div class="imgCat"></div>
            <h2>Videogiochi PC</h2>
            <p>Per i più geek, acquista i più famosi giochi tripla A per PC facilmente e ad un prezzo minore</p>
        </div>
    </a>   
</div>
<script src="js/manager_script.js"></script>
<script src="js/logout_script.js"></script>
<?php include("bottom.html"); ?>