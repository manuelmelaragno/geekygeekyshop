<?php 
    include("top.html");
    include("./common.php");

    if(isset($_SESSION["id_utente"])){
        header("location: ./home.php");
        die();
    }
?>
    <div class="container">

        <div class="jumbotron" id="jumbotron">
            <h1 class="display-4">Benvenuto in Geeky Geeky Shop!</h1>
            <p class="lead">L'e-commerce di videogames, film e serie tv per i più appassionati! Effettua il log in oppure registrati per dare un'occhiata alle merci.  </p>
            <hr class="my-4">
            <p>Buona permanenza!</p>
        </div>

        <div class="alert alert-danger" role="alert" id="errorDiv">
            
        </div>
        
            <div class="card-deck">
                <div class="card">
                <div class="card-body">
                    <form class="form">
                        <div class="form-group">
                            <label for="nomeReg">Nome Utente</label>
                            <input name="nome" type="text" class="form-control" id="nomeReg" placeholder="Inserisci nome utente">
                        </div>
                        <div class="form-group">
                            <label for="emailReg">Email</label>
                            <input name="email" type="email" class="form-control" id="emailReg" aria-describedby="emailHelpReg" placeholder="Inserisci email">
                            <small id="emailHelpReg" class="form-text text-muted">Non condividiamo l' e-mail con nessun servizio di terze parti.</small>
                        </div>
                        <div class="form-group">
                            <label for="passwordReg">Password</label>
                            <input name="password" type="password" class="form-control" id="passwordReg" placeholder="Inserisci password">
                        </div>
                        <div class="form-group">
                            <label for="passwordReg">Ripeti password</label>
                            <input name="ripetiPassword" type="password" class="form-control" id="ripetiPasswordReg" placeholder="Ripeti password">
                        </div>
                        <button id="btnRegistrati" type="submit" class="btn btn-primary index_form_buttons">Registrati</button>
                    </form>
                </div>
                </div>
                
                <div class="card">
                <div class="card-body">
                    <form class="form">
                        <div class="form-group">
                            <label for="emailLogin">Email</label>
                            <input name="email" type="email" class="form-control" id="emailLogin" aria-describedby="emailHelpLogIn" placeholder="Enter email">
                            <small id="emailHelpLogIn" class="form-text text-muted">Non condividiamo l' e-mail con nessun servizio di terze parti.</small>
                        </div>
                        <div class="form-group">
                            <label for="passwordLogin">Password</label>
                            <input name="password" type="password" class="form-control" id="passwordLogin" placeholder="Password">
                        </div>
                        <button id="btnLogin" type="submit" class="btn btn-primary index_form_buttons">Log-In</button>
                    </form>
                </div>
                </div>
            </div>
    </div>
<script src="js/index_script.js"></script>
<?php include("bottom.html"); ?>