var btnLogin;
var btnRegistrati;

$("#errorDiv").hide();
$("#logout").hide();

function onLoad(){
    btnLogin = document.getElementById("btnLogin");
    btnRegistrati = document.getElementById("btnRegistrati");

    btnLogin.addEventListener("click", login);
    btnRegistrati.addEventListener("click", registrati);
    
    $(".form").bind("submit", function(){
        event.preventDefault(); 
    });
}

function login(){

    var email = $("#emailLogin").val();
    var password = $("#passwordLogin").val();

    $.post("./utenti/utenti.php", { metodo:"login", emailLogin: email, passwordLogin: password }, "json")
        .done(function (data) {

            data = JSON.parse(data);

            if (data.status == "errore") { //ERRORE UTENTE NON REGISTRATO

                $("#errorDiv").removeClass("alert-success").addClass("alert-danger");
                $("#errorDiv").text("Log in incorretto. Controllare le credenziali e riprovare");
                $("#errorDiv").show();

            } else if (data.status == "successo") {

                $("#errorDiv").removeClass("alert-danger").addClass("alert-success");
                $("#errorDiv").text("Bentornato : " + data.email);
                $("#errorDiv").show();
                window.location.replace("./home.php");
            }
        });
}

function registrati(){
    var email = $("#emailReg").val();
    var password = $("#passwordReg").val();
    var nome = $("#nomeReg").val();
    var ripetiPassword = $("#ripetiPasswordReg").val();

    if (email.indexOf("@") > 0 & email.indexOf(".") > email.indexOf("@") && password != "" && nome !="" && ripetiPassword != "" && password == ripetiPassword){

        $.post("./utenti/utenti.php", { metodo: "registra", emailReg: email, passwordReg: password, nomeReg: nome }, "json")
            .done(function (data) {

                data = JSON.parse(data);

                if (data.status == "errore") { //ERRORE UTENTE NON REGISTRATO

                    $("#errorDiv").removeClass("alert-success").addClass("alert-danger");
                    $("#errorDiv").text("Registrazione non riuscita, utente già registrato.");
                    $("#errorDiv").show();
                    console.log(data);

                } else if (data.status == "successo") {

                    $("#errorDiv").removeClass("alert-danger").addClass("alert-success");
                    $("#errorDiv").text("Registrazione riuscita! Ora puoi effettuare il Log in.");
                    $("#errorDiv").show();

                    console.log(data);
                }
            });
    }else{
        $("#errorDiv").removeClass("alert-success").addClass("alert-danger");
        $("#errorDiv").text("Registrazione non riuscita, controlla i dati.");
        $("#errorDiv").show();
    }
}

window.onload = onLoad;