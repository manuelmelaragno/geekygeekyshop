$(".snackBar").hide();

function loadProdotti(url,tipo){
    $.post(url, { tipo:tipo }, "json")
        .done(function (data) {

            data = JSON.parse(data);

            var text = '';
            dataCounter = 0;

            for(var i=0; i<data.length; i++){
                text += '<div class="carta">\n';
                text += '<a href="./prodotto.php?idP='+data[i]["id"]+'" class="prodotto"><img class="coverCarta" src="' + data[i]["url_img"] +'" alt=""></a>\n';
                text += '<p class="lead nome"><a href="./prodotto.php?idP='+data[i]["id"]+'">' + data[i]["nome"] + '</a></p>\n';
                text += '<p class="prezzo">' + data[i]["prezzo"]+'€ </p>\n';
                text += '<p class="id" hidden>'+data[i]["id"]+'</p>\n';
                if(tipo != "Carrello" && tipo != "Wish List"){
                    text += '<div class="iconaRightBottom" title="Aggiungi alla Wish List"><a href="#"><i class="material-icons"> whatshot </i></a></div>';
                    text += '<div class="iconaRightBottom2" title="Aggiungi al Carrello"><a href="#"><i class="material-icons">add_shopping_cart</i></a></div>';
                }
                else if(tipo == "Carrello")
                    text += '<div class="iconaRightBottom" title="Rimuovi dal carrello"><a href="#"><i class="material-icons"> close </i></a></div>';
                else if(tipo == "Wish List")
                    text += '<div class="iconaRightBottom" title="Rimuovi dalla Wish List"><a href="#"><i class="material-icons"> close </i></a></div>';
                text += '</div>\n';
            }
        
                if(tipo=="Carrello")
                    text += '<a href="#" class="btn btn-primary btn-lg disabled carrelloCheckOut" tabindex="-1" role="button" aria-disabled="true">Procedi al CheckOut</a>';

            $("#homeContainer").html($("#homeContainer").html() + text);
        
        
            $(".iconaRightBottom").bind("click", function(){
                event.preventDefault(); 
            });
            $(".iconaRightBottom2").bind("click", function(){
                event.preventDefault(); 
            });
            
            if(tipo != "Wish List" && tipo != "Carrello"){
                $(".iconaRightBottom").bind("click", function(){
                    aggiungiAllaWishList(tipo, $(this).parent());
                });
                $(".iconaRightBottom2").bind("click", function(){
                    aggiungiAlCarrello(tipo, $(this).parent());
                });
            }else if(tipo == "Wish List"){
                $(".iconaRightBottom").bind("click", function(){
                    rimuoviWishList($(this).parent());
                });    
            }else if(tipo == "Carrello"){
                $(".iconaRightBottom").bind("click", function(){
                    rimuoviCarrello($(this).parent());
                }); 
            }

            if(tipo != "Carrello"){
                $(".carta").draggable({
                    helper: "clone",
                });

                $("#aggiungiAlCarrello").droppable({

                    tolerance : "touch",

                    over: function(event, ui){
                        $(ui.helper).css("border-bottom","10px solid #00ff36");
                    },

                    out: function(event, ui){
                        $(ui.helper).css("border-bottom","0px solid green");
                    },

                    drop: function( event, ui ) {
                        aggiungiAlCarrello(tipo, $(ui.draggable));
                      }
                });
            }

        });
    
}

function aggiungiAlCarrello(tipo, prodotto){
    
    var idP = prodotto.find(".id").text();
    
    $.post("./carrello/agg_carrello.php", { idProdotto: idP }, "json")
    .done(function(data){
        data = JSON.parse(data);
        if(data.status == "errore"){
            
            $(".snackBar").find("p").text(tipo +" già nel carrello!");
            $(".snackBar").fadeIn("fast");
            
            setTimeout(function() {
                $(".snackBar").fadeOut("fast");
            }, 2000);
            
        }else{
            
            $(".snackBar").find("p").text(tipo +" aggiunto al carrello!");
            $(".snackBar").fadeIn("fast");
            
            setTimeout(function() {
                $(".snackBar").fadeOut("fast");
            }, 2000);
            
        }
    });
}

function aggiungiAllaWishList(tipo, prodotto){
    
    var idP = prodotto.find(".id").text();
    
    $.post("./wishlist/agg_wishlist.php", { idProdotto: idP }, "json")
    .done(function(data){
        data = JSON.parse(data);
        if(data.status == "errore"){
            
            $(".snackBar").hide();
            $(".snackBar").find("p").text(tipo + " già nella Wish List!");
            $(".snackBar").fadeIn("fast");
            
            setTimeout(function() {
                $(".snackBar").fadeOut("fast");
            }, 2000);
        }else{
            
            $(".snackBar").hide();
            $(".snackBar").find("p").text(tipo + " aggiunto alla Wish List!");
            $(".snackBar").fadeIn("fast");
            
            setTimeout(function() {
                $(".snackBar").fadeOut("fast");
            }, 2000);
            
        }
    });
}

function rimuoviWishList(film){
    
    var idP = film.find(".id").text();
    
    $.post("./wishlist/rim_wishlist.php", { idProdotto:idP }, "json")
        .done(function (data) 
              {
                window.location.replace("./wishlist.php");
              });
}

function rimuoviCarrello(film){
    
    var idP = film.find(".id").text();
    
    $.post("./carrello/rim_carrello.php", { idProdotto:idP }, "json")
        .done(function(data){
        window.location.replace("./carrello.php");
    });
    
    
}
