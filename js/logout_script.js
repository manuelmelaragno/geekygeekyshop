var logout;

function setLogout(){
    logout = document.getElementById("logout");

    logout.addEventListener("click", function(){
        $.post("./common.php", { logout: "logout"}, "json")
            .done(function (data) {
                window.location.replace("./index.php");
            });
    });
}