function onLoad() {
    
    setLogout();
    
    var tipo = "";
    var url;
    var page = window.location.pathname.split('/').pop();
    
    if(page == "films.php"){
        
        tipo = "Film";
        url = "./prodotto/get_prodotti.php";
        
    }else if(page == "videogiochi.php"){
        
        tipo = "Videogioco";
        url = "./prodotto/get_prodotti.php";
        
    }else if(page == "serietv.php"){
        
        tipo = "Serie TV";
        url = "./prodotto/get_prodotti.php";
        
    }else if(page == "wishlist.php"){
        
        tipo = "Wish List";
        url = "./wishlist/get_wishlist.php";
        
    }else if(page == "carrello.php"){
        
        tipo = "Carrello";
        url = "./carrello/get_carrello.php";
        
    }
    
    if(tipo != ""){
        loadProdotti(url, tipo);
    }
}


window.onload = onLoad;