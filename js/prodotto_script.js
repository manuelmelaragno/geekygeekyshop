$(".snackBar").hide();
var btnInviaRecensione;

function onLoad() {
    
    var idP = $("#idP").text();
    $("#idP").remove();
    
    loadInfo(idP);
    loadRecensioni(idP);
    
    $("#btnInviaRecensione").bind("click", function(){inviaRecensione(idP,$("#recensioneProdotto").val())});
    
    setLogout();
    
    $("#btnAggiungiCarrello").bind("click", function(){
        aggiungiAlCarrello(idP);
    })
    
    $("#btnAggiungiWishList").bind("click", function(){
        aggiungiAllaWishList(idP);
    })
}

function loadInfo(idP){
    
    $.post("./prodotto/get_prodotto.php", { idProdotto:idP }, "json")
        .done(function (data) {

            data = JSON.parse(data);

            $("#titoloProdotto").text(data.nome);
            $("#genereProdotto").text("Genere - " + data.tipo);
            $("#coverProdotto").attr("src", data.url_img);
            $("#prezzoProdotto").text("Prezzo - " + data.prezzo + "€");
            $("#descProdotto").text(data.descrizione);

        });
}

function loadRecensioni(idP){
    $.post("./recensioni/get_recensioni.php", { idProdotto:idP }, "json")
        .done(function (data) {

            data = JSON.parse(data);

            var text = '';

            for(var i=0; i<data.length; i++){
                text += '<blockquote class="blockquote recensione">\n';
                text += '<p class="mb-0">'+data[i].testo+'</p>\n';
                text += '<footer class="blockquote-footer">'+data[i].nomeUtente+'</footer>\n';
                text += '</blockquote>\n';
            }
            
        
            $("#recensioni").html($("#recensioni").html() + text);

        });
    
}

function aggiungiAlCarrello(idP){
    $.post("./carrello/agg_carrello.php", { idProdotto: idP }, "json")
    .done(function(data){
        data = JSON.parse(data);
        if(data.status == "errore"){
            
            $(".snackBar").find("p").text("Film già nel carrello!");
            $(".snackBar").fadeIn("fast");
            
            setTimeout(function() {
                $(".snackBar").fadeOut("fast");
            }, 2000);
            
        }else{
            
            $(".snackBar").find("p").text("Film aggiunto al carrello!");
            $(".snackBar").fadeIn("fast");
            
            setTimeout(function() {
                $(".snackBar").fadeOut("fast");
            }, 2000);
            
        }
    });
}

function aggiungiAllaWishList(idP){
    
    $.post("./wishlist/agg_wishlist.php", { idProdotto: idP }, "json")
    .done(function(data){
        data = JSON.parse(data);
        if(data.status == "errore"){
            
            $(".snackBar").find("p").text("Film già nella Wish List!");
            $(".snackBar").fadeIn("fast");
            
            setTimeout(function() {
                $(".snackBar").fadeOut("fast");
            }, 2000);
        }else{
            
            $(".snackBar").find("p").text("Film aggiunto alla Wish List!");
            $(".snackBar").fadeIn("fast");
            
            setTimeout(function() {
                $(".snackBar").fadeOut("fast");
            }, 2000);
            
        }
    });
}

function inviaRecensione(idP, testo){
    $.post("./recensioni/agg_recensione.php", { idProdotto:idP, testoRecensione:testo }, "json")
        .done(function (data) {

            data = JSON.parse(data);
            console.log(data);

            
            if(data.status == "successo"){

                $(".snackBar").find("p").text("Recensione inviata!");
                $(".snackBar").fadeIn("fast");

                setTimeout(function() {
                    $(".snackBar").fadeOut("fast");
                }, 2000);
                
                location.reload();

            }else{

                $(".snackBar").find("p").text("Errore, recensione non inviata!");
                $(".snackBar").fadeIn("fast");

                setTimeout(function() {
                    $(".snackBar").fadeOut("fast");
                }, 2000);

            }

        });
}

window.onload = onLoad;