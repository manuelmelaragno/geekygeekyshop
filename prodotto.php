<?php 
include("top.html");
include("./common.php");

if (!isset($_SESSION["id_utente"])) {
    header("location: ./index.php");
    die();
}

?>
<div class="container" id="homeContainer">
    
    <p id="idP"><?= $_GET["idP"] ?></p>
    <img id="coverProdotto" alt="" class="prodottoImgCover" src="./" width="350" height="500">
    <h1 id="titoloProdotto" class="display-1">Titolo</h1>
    <p id="genereProdotto" class="text-muted">Genere</p>
    <p id="descProdotto" class="lead"></p>
    <p id="prezzoProdotto" class="h4 prodottoPrezzo">Prezzo - </p>
    
    <button id="btnAggiungiCarrello" type="button" class="btn btn-primary btn-lg">Aggiungi al carrello</button>
    <button id="btnAggiungiWishList" type="button" class="btn btn-success btn-lg">Aggiungi alla Wish List</button>

    <hr class=" prodottoHr">
    
    <p class="h3">Aggiungi una recensione!</p>
    <textarea id="recensioneProdotto" class="form-control textarea" rows="5"></textarea>
    <button id="btnInviaRecensione" type="button" class="btn btn-outline-primary btn-lg btnAggRecensione">Invia</button>
    
    <div id="recensioni">
        <p class="h4">Recensioni</p>
    </div>
</div>


<div class="snackBar"><p class="lead"></p></div>


<script src="js/logout_script.js"></script>
<script src="js/prodotto_script.js"></script>
<?php include("bottom.html"); ?>