<?php
include("../common.php");

if(isset($_POST["tipo"])){
    $db = connect();

    $tipo = $db->quote($_POST["tipo"]);

    $rows = $db->query("SELECT * FROM prodotto WHERE tipo = $tipo;");
    $values = array();

    if ($rows->rowCount() > 0) {
        $i = 0;
        foreach($rows as $row){
            $values[$i]["id"] = $row["id"];
            $values[$i]["nome"] = $row["nome"];
            $values[$i]["tipo"] = $row["tipo"];
            $values[$i]["url_img"] = $row["url_img"];
            $values[$i]["prezzo"] = $row["prezzo"];
            $i++;
        }

        echo json_encode($values);
    }
}

?>