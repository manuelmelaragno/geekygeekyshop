<?php
include("../common.php");

$values = array();
$values["status"] = "errore";

if(isset($_POST["idProdotto"])){
    
    $db = connect();

    $idP = $db->quote($_POST["idProdotto"]);

    $rows = $db->query("SELECT * FROM prodotto WHERE id = $idP;");

    if ($rows->rowCount() > 0) {
        
        $row = $rows->fetch();
                
        $values["status"] = "successo";
        $values["id"] = $row["id"];
        $values["nome"] = $row["nome"];
        $values["tipo"] = $row["tipo"];
        $values["descrizione"] = $row["descrizione"];
        $values["url_img"] = $row["url_img"];
        $values["prezzo"] = $row["prezzo"];
    }
    
}

echo json_encode($values);
?>