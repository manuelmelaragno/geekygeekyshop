<?php
include("../common.php");

$values = array();
$values["status"] = "errore";

if(isset($_POST["idProdotto"])){
    
    $db = connect();

    $idP = $db->quote($_POST["idProdotto"]);
    $idU = $db->quote($_SESSION["id_utente"]);
    $testo = $db->quote($_POST["testoRecensione"]);

    $db->query("INSERT INTO recensione(id_utente, id_prodotto, testo) VALUES($idU, $idP, $testo)");
    
    $values["status"] = "successo";
}

echo json_encode($values);
?>