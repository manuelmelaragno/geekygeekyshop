<?php
include("../common.php");

$values = array();

if(isset($_POST["idProdotto"])){
    
    $db = connect();
    $idP = $db->quote($_POST["idProdotto"]);

    $rows = $db->query("SELECT r.testo AS testo, u.nome AS nome FROM recensione r 
                        JOIN utente u ON r.id_utente = u.id 
                        JOIN prodotto p on r.id_prodotto = p.id 
                        WHERE id_prodotto = $idP;");

    if ($rows->rowCount() > 0) {
        $i = 0;
        foreach($rows as $row){
            $values[$i]["nomeUtente"] = $row["nome"];
            $values[$i]["testo"] = $row["testo"];
            $i++;
        }
    }
}

echo json_encode($values);

?>