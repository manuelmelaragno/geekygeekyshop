<?php 
include("top.html");
include("./common.php");

if (!isset($_SESSION["id_utente"])) {
    header("location: ./index.php");
    die();
}
?>
<div class="container" id="homeContainer">

    <div class="jumbotron" id="jumbotron">
        <h1 class="display-4">Ecco l'elenco delle serie tv!</h1>
        <hr class="my-4">
        <p class="lead">Ora scegli una serie per vederne la descrizione e le recensioni, aggiungilo alla tua wishlist oppure trascinalo nel carrello!</p>
    </div>
    
    <div id="aggiungiAlCarrello"><img alt="" src="img/aggiungi_al_carrello.png"></div>
    
</div>
<div class="snackBar"><p class="lead"></p></div>


<!--<script src="js/serietv_script.js"></script>-->
<script src="js/manager_script.js"></script>
<script src="js/load_prodotti.js"></script>
<script src="js/logout_script.js"></script>
<?php include("bottom.html"); ?>