<?php
    include("../common.php");

    $values = array();
    $values["status"] = "errore";

    if(isset($_POST["metodo"]) && $_POST["metodo"] == "login"){ //EFFETTUO IL LOGIN
        if(isset($_POST["emailLogin"]) && isset($_POST["passwordLogin"])){

            $db = connect();
            
            $email = $db->quote($_POST["emailLogin"]);
            $password = $db->quote($_POST["passwordLogin"]);
            
            $rows = $db->query("SELECT * FROM utente WHERE email = $email");
            $values = array();

            if ($rows->rowCount() > 0) {
                $first_row = $rows->fetch();
                if(password_verify($password, $first_row["password"])){
                    $values["status"] = "successo";

                    $_SESSION["id_utente"] = $first_row["id"];
                    $_SESSION["email"] = $first_row["email"];
                    $_SESSION["nome"] = $first_row["nome"];
                }else{
                    $values["status"] = "errore";
                }
            }else{
                $values["status"] = "errore";
            }
        }
    } else if(isset($_POST["metodo"]) && $_POST["metodo"] == "registra"){ //EFFETTUO LA REGISTRAZIONE

        if(isset($_POST["emailReg"]) && isset($_POST["passwordReg"]) && isset($_POST["nomeReg"])){

            $db = connect();

            $email = $db->quote($_POST["emailReg"]);
            $password = $db->quote($_POST["passwordReg"]);
            $nome = $db->quote($_POST["nomeReg"]);
            
            $hash = password_hash($password, PASSWORD_DEFAULT);
            $hash = $db->quote($hash);

            $rows = $db->query("SELECT * FROM utente WHERE email = $email OR nome = $nome");

            if ($rows->rowCount() > 0) { // C'è gia un utente con queste informazioni

                $values["status"] = "errore";

            } else {

                $db->query("INSERT INTO utente(email, password, nome) VALUES($email, $hash, $nome)");

                $values["status"] = "successo";
            }
        }
    }
    echo json_encode($values);
?>