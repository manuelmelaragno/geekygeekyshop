<?php 
include("top.html");
include("./common.php");

if (!isset($_SESSION["id_utente"])) {
    header("location: ./index.php");
    die();
}
?>
<div class="container" id="homeContainer">

    <div class="jumbotron" id="jumbotron">
        <h1 class="display-4">Ecco la tua Wish List!</h1>
        <hr class="my-4">
        <p class="lead">Da qui puoi trascinare per aggiungere al carrello un film oppure rimuoverlo dalla tua Wish List.</p>
    </div>
    
    <div id="aggiungiAlCarrello"><img alt="" src="img/aggiungi_al_carrello.png"></div>
    
</div>
<div class="snackBar"><p class="lead"></p></div>

<!--<script src="js/wishlist_script.js"></script>-->
<script src="js/manager_script.js"></script>
<script src="js/load_prodotti.js"></script>
<script src="js/logout_script.js"></script>
<?php include("bottom.html"); ?>